sourceif () { [ -f "$1" ] && \. "$1"; }

sourceif "/etc/bashrc"

usable () { command -v $1 > /dev/null; }
atpide () { [ -d "$1" ] && export PATH="$1:$PATH"; }

export PS1='[\[\e[94m\]\u@\h \W\[\e[39m\]]\$ '

usable make && export MAKEFLAGS="-j$(nproc)"
usable java && export JAVACMD="java"
usable drip && export JAVACMD="drip"
usable vim  && export EDITOR="vim -e"
usable vim  && export VISUAL="vim"
usable go   && export GOROOT="$HOME/usr/local/go"
usable lynx && export LYNX_LSS="$HOME/.lynx.lss"

atpide "$HOME/.local/bin"
atpide "$HOME/usr/bin"
usable cabal && export PATH="$HOME/.cabal/bin:$PATH"
usable go    && export PATH="$GOROOT/bin:$PATH"

usable ruby && for d in $HOME/.gem/ruby/*/bin;
  do export PATH="$d:$PATH"; done

set +o ignoreeof

alias ls='ls --color=auto'
alias ll='ls -l'
alias lA='ls -A'
usable cowsay     && alias cowsay=':'
usable vim        && alias vi='vim'
usable vim        && alias vim='vim -O'
usable vim        && alias view='vim -R'
usable hub        && alias git='hub'
usable acpi       && alias battery='acpi -bi'
usable apacman    && alias pacman='sudo apacman'
usable stacscheck && alias sc='stacscheck'
alias java="$JAVACMD"

# makes a sym-link to folder "current" in home directory:
set-current () { ln -svfT $(pwd)/$1 $HOME/current; }

# prints something to stderr:
echoerr () { echo "$@" 1>&2; }

# run scripts installed in local npm_modules:
if usable npm; then
  npm-do () { (PATH=$(npm bin):$PATH; eval $@;) }
fi

# loads nvm on first use of node or npm:
NVM_DIR="$HOME/.nvm"
if [ -d "$NVM_DIR" ]; then
  export NVM_DIR
  alias load-nvm='echoerr "Initialising nvm..." && \
    unalias node npm nvm load-nvm npm-do && \. "$NVM_DIR/nvm.sh"'
  alias node='load-nvm && node'
  alias npm='load-nvm && npm'
  alias nvm='load-nvm && nvm'
  alias npm-do='load-nvm && npm-do'
fi
