runtime bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()

runtime! plugin/sensible.vim

syntax enable
set background=dark
colorscheme solarized

filetype plugin indent on
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

" line numbering
" set number
highlight LineNr ctermfg=grey

" character margin
set colorcolumn=77
highlight ColorColumn ctermbg=darkgrey

" code folding
" set fdm=syntax

" disable arrow keys
inoremap <Up>    <NOP>
inoremap <Down>  <NOP>
inoremap <Left>  <NOP>
inoremap <Right> <NOP>
noremap  <Up>    <NOP>
noremap  <Down>  <NOP>
noremap  <Left>  <NOP>
noremap  <Right> <NOP>

" allow unsaved buffers in background
" useful according to
" https://stackoverflow.com/questions/102384/using-vims-tabs-like-buffers
set hidden

" vim-gfm-syntax emoji concealing:
let g:gfm_syntax_emoji_conceal = 1
